# Author-Dag Rende, Anders Jönsson
# Description-Import a Wing Profile into a Fusion360 sketch


import os
import sys
from collections import defaultdict

import adsk.core
import adsk.fusion
import traceback

from . import utils
from . import addin
from .lib import positioningPath
from .lib import gcodeWriter
from .lib import plane
from .lib import point
from .lib import expand


def fieldValidator(addIn: addin.AddIn, args):
    cmd = args.firingEvent.sender

    # define attribute validation (part 4 of 5)
    for input in cmd.commandInputs:
        if input.id == 'rootAirFoil':
            if input.value and len(input.value) > 0:
                if not os.path.isfile(input.value):
                    args.areInputsValid = False
            else:
                args.areInputsValid = False
        elif input.id == 'tipAirFoil':
            if input.value and len(input.value) > 0:
                if not os.path.isfile(input.value):
                    args.areInputsValid = False


def inputChanged(addIn, args):
    pass

# create points for the foils on two planes that are rootTipDistance apart
# create a loft between the two foils to visualize the wing

def executor(addIn: addin.AddIn) -> None:
    addIn.log('Running on Python version ' + sys.version)  

    if not addIn.design:
        raise RuntimeError('No active Fusion design')
    addIn.log('execute', addIn.attributes)

    # Foam body, a Fusion 360 model of the foam to cut
    foamBody = addIn.attributes['foamBody'][0]

    #addIn.log('Profile')      
    # Profile
    leftProfile = addIn.attributes['leftProfile'][0]
    rightProfile = addIn.attributes['rightProfile'][0]
    leftProfileSketch = leftProfile.parentSketch
    rightProfileSketch = rightProfile.parentSketch
    # Create intersection (infinite) lines through the left and right profiles.
    profileProjectionLines = getProjectionLinesFromProfiles(leftProfileSketch, rightProfileSketch)
    # Get intersections with CNC planes
    leftCncPlane = addIn.attributes['leftCncPlane'][0]
    rightCncPlane = addIn.attributes['rightCncPlane'][0]
    leftCncProfilePoints = getIntersectionPoints(leftCncPlane, profileProjectionLines)
    rightCncProfilePoints = getIntersectionPoints(rightCncPlane, profileProjectionLines)
    
    #addIn.log('Positioning Paths')      
    # Positioning Path
    leftPositioningSketchLines: adsk.fusion.SketchLines = addIn.attributes['leftPositioningPath']
    rightPositioningSketchLines: adsk.fusion.SketchLines = addIn.attributes['rightPositioningPath']
    # Logging
    # addIn.log("Left positioning sketch segments:")
    # for lineSegment in leftPositioningSketchLines:
    #         p3d = lineSegment.startSketchPoint.worldGeometry
    #         addIn.log("lineSegment.start3dPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
    #         p3d = lineSegment.endSketchPoint.worldGeometry
    #         addIn.log("lineSegment.end3dPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))

    # addIn.log("Right positioning sketch segments:")
    # for lineSegment in rightPositioningSketchLines:
    #         p3d = lineSegment.startSketchPoint.worldGeometry
    #         addIn.log("lineSegment.start3dPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
    #         p3d = lineSegment.endSketchPoint.worldGeometry
    #         addIn.log("lineSegment.end3dPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))

    leftPositioningPath = positioningPath.PositioningPath(leftPositioningSketchLines, foamBody, addIn.log)
    rightPositioningPath = positioningPath.PositioningPath(rightPositioningSketchLines, foamBody, addIn.log)
     # Logging
    # addIn.log("Left positioning points:")
    # for p3d in leftPositioningPath.getPoint3ds():
    #         addIn.log("Left positioning point "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
    # addIn.log("Right positioning points:")
    # for p3d in rightPositioningPath.getPoint3ds():
    #         addIn.log("Right positioning point "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))

    positioningPathProjectionLines = getProjectionLinesFromPositioningPaths(leftPositioningPath, rightPositioningPath)
    # addIn.log("Number of positioning path projection lines", str(len(positioningPathProjectionLines)))
    leftCncPositioningPoints = getIntersectionPoints(leftCncPlane, positioningPathProjectionLines)
    rightCncPositioningPoints = getIntersectionPoints(rightCncPlane, positioningPathProjectionLines)
     # Logging
    # addIn.log("Left positioning CNC points:")
    # for p3d in leftCncPositioningPoints:
    #         addIn.log("Left CNC positioning point "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
    # addIn.log("Right positioning CNC points:")
    # for p3d in rightCncPositioningPoints:
    #         addIn.log("Right CNC positioning point "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))

    addIn.log("Creating heat start projection line")
    # Find point where heat should be turned on
    # Find first point where heat must be turned on
    # leftHeatStartPointNumber = leftPositioningPath.getHeatStartPointNumber()
    # rightHeatStartPointNumber = rightPositioningPath.getHeatStartPointNumber()
    # if(leftHeatStartPointNumber <= rightHeatStartPointNumber):
    #     heatStartPointNumber = leftHeatStartPointNumber
    # else:
    #     heatStartPointNumber = rightHeatStartPointNumber
    addIn.log('leftHeatStartPointNumber =  ' + str(leftPositioningPath.getHeatStartPointNumber()))
    addIn.log('rightHeatStartPointNumber =  ' + str(rightPositioningPath.getHeatStartPointNumber()))
    heatStartPointNumber = min(leftPositioningPath.getHeatStartPointNumber(), rightPositioningPath.getHeatStartPointNumber())
    addIn.log('heatStartPointNumber =  ' + str(heatStartPointNumber))
    leftHeatStartPoint3D = leftPositioningPath.getPoint3d(heatStartPointNumber)
    rightHeatStartPoint3D = rightPositioningPath.getPoint3d(heatStartPointNumber)
    heatStartProjectionLine = getProjectionLineFromPoint3ds(leftHeatStartPoint3D, rightHeatStartPoint3D)
    leftCNCHeatStartPoint = getIntersectionPoint(leftCncPlane, heatStartProjectionLine)
    rightCNCHeatStartPoint = getIntersectionPoint(rightCncPlane, heatStartProjectionLine)
    
    # Log heat start points at the CNC plane
    p3d = leftCNCHeatStartPoint
    addIn.log('Left heat start CNC point' + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
    p3d = rightCNCHeatStartPoint
    addIn.log('Right heat start CNC point' + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))


    # Visualization of cutting patterns
    addIn.log("Visualization of cutting patterns")
    # Create sketches just to show the left and right projected CNC profiles on the left and right CNC planes.
    sketches = addIn.rootComp.sketches
    leftCncSKetch = sketches.add(leftCncPlane)
    rightCncSketch = sketches.add(rightCncPlane)
    leftCncSKetch.attributes.add("WireCutterProjector", "profile", "leftCncSketch")
    rightCncSketch.attributes.add(" WireCutterProjector", "profile", "leftCncSketch")
    leftCncSKetch.name = "CNC_left_(x,y)"
    rightCncSketch.name = "CNC_right_(a,b)"
    # Draw the profile to cut. Just to visualize it.
    createProfile(leftCncSKetch, leftCncProfilePoints)
    createProfile(rightCncSketch, rightCncProfilePoints)
    # Draw positioning lines. Just to visualize it.
    createLinePath(leftCncSKetch, leftCncPositioningPoints)
    createLinePath(rightCncSketch, rightCncPositioningPoints)

    # gcode generation...

    # gcode is generated for the left and right 2-axis cutter head planes.
    # 3d points on the CNC planes defined in Fusion 360 must be translated into wire cutter 2d planes
    # All coordinates in the 2D plane must then be translated so that the very first point (where the machine starts)
    # is (0,0) and all other points are then given relative to this new cutter plane origo.

    # Create the 2D planes
    leftCnc2DPlane = plane.Plane(leftCncPlane, addIn.log)
    rightCnc2DPlane = plane.Plane(rightCncPlane, addIn.log)

    # # Sanity check. The 3D points must lie on the 2D plane
    # if(not leftCnc2DPlane.isPointsOnPlane(leftCncProfilePoints)):
    #     raise RuntimeError('Left CNC 3D profile points are not on the 2D plane')
    # if(not rightCnc2DPlane.isPointsOnPlane(rightCncProfilePoints)):
    #     raise RuntimeError('Right CNC 3D profile points are not on the 2D plane')
    # if(not leftCnc2DPlane.isPointsOnPlane(leftCncPositioningPoints)):
    #     raise RuntimeError('Left CNC 3D positioning points are not on the 2D plane')
    # if(not rightCnc2DPlane.isPointsOnPlane(rightCncPositioningPoints)):
    #     raise RuntimeError('Right CNC 3D positioning points are not on the 2D plane')

    # Convert 3D coordinates to CNC plane 2D coordinates
    leftCnc2DProfilePoints: list[point.Point] = leftCnc2DPlane.getPointsInPlaneCoordinates(leftCncProfilePoints)
    rightCnc2DProfilePoints: list[point.Point] = rightCnc2DPlane.getPointsInPlaneCoordinates(rightCncProfilePoints)
    leftCnc2DPositioningPoints: list[point.Point] = leftCnc2DPlane.getPointsInPlaneCoordinates(leftCncPositioningPoints)
    rightCnc2DPositioningPoints: list[point.Point] = rightCnc2DPlane.getPointsInPlaneCoordinates(rightCncPositioningPoints)
    leftCnc2DHeatStartPoint: point.Point = leftCnc2DPlane.getPointInPlaneCoordinates(leftCNCHeatStartPoint)
    rightCnc2DHeatStartPoint: point.Point = rightCnc2DPlane.getPointInPlaneCoordinates(rightCNCHeatStartPoint)

    # Logging
    # addIn.log("Left CNC 2D profile points:")
    # for p2d in leftCnc2DProfilePoints:
    #     addIn.log("Left CNC 2D profile point "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Right CNC 2D profile points:")
    # for p2d in rightCnc2DProfilePoints:
    #     addIn.log("Right CNC 2D profile point "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Left CNC 2D positioning points:")
    # for p2d in leftCnc2DPositioningPoints:
    #     addIn.log("Left 2D CNC positioning point "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Right CNC 2D positioning points:")
    # for p2d in rightCnc2DPositioningPoints:
    #     addIn.log("Right 2D CNC positioning point "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Left 2D Heat point "  + str(leftCnc2DHeatStartPoint.x) + ", " + str(leftCnc2DHeatStartPoint.y) + ", " + str(leftCnc2DHeatStartPoint.z))
    # addIn.log("Right 2D Heat point "  + str(rightCnc2DHeatStartPoint.x) + ", " + str(rightCnc2DHeatStartPoint.y) + ", " + str(rightCnc2DHeatStartPoint.z))


    # Create gcode from 2D points
    writeGCode(leftCnc2DProfilePoints, rightCnc2DProfilePoints, leftCnc2DPositioningPoints, rightCnc2DPositioningPoints, leftCnc2DHeatStartPoint, rightCnc2DHeatStartPoint) 

# write gcode to file gcode.txt
# test result with https://ncviewer.com/
def writeGCode(leftCncProfilePoints: list[point.Point], rightCncProfilePoints: list[point.Point], leftCncPositioningPoints: list[point.Point], rightCncPositioningPoints: list[point.Point], leftHeatStartPoint: point.Point, rightHeatStartPoint: point.Point):
    # Sanity checks
    if (not len(leftCncProfilePoints) == len(rightCncProfilePoints)):
        utils.messageBox('Left and right profile point counts are not the same!')
        raise RuntimeError('Left and right profile point counts are not the same!')
    if (not len(leftCncPositioningPoints) == len(rightCncPositioningPoints)):
        utils.messageBox('Left and right profile point counts are not the same!')
        raise RuntimeError('Left and right positioning point counts are not the same!')
    # Check that the profiles start at the same point as the positioning paths end, and if they don't, reorder them so they do
    try:
        leftCncProfilePoints = reorderPointList(leftCncProfilePoints, leftCncPositioningPoints[-1])
        rightCncProfilePoints = reorderPointList(rightCncProfilePoints, rightCncPositioningPoints[-1])
    except:
        addIn.log(traceback.format_exc())
        utils.messageBox(traceback.format_exc())
   
    # All gcode coordinates must be relative to the start position of the machine.
    # The start position of the machine, i.e the origo of coordinates, is assumed to be (in fact this *MUST* be the case) 
    # at the first point of the positioning path.

    # Thus the first point of the positioning path should be set to the origo and all other points must be translated to be relative to that point.
    leftOrigo = leftCncPositioningPoints[0]
    rightOrigo = rightCncPositioningPoints[0]

    leftRelativeProfilePoints: list[point.Point] = translatePoints(leftOrigo, leftCncProfilePoints)
    rightRelativeProfilePoints: list[point.Point] = translatePoints(rightOrigo, rightCncProfilePoints)
    leftRelativePositioningPoints: list[point.Point] = translatePoints(leftOrigo, leftCncPositioningPoints)
    rightRelativePositioningPoints: list[point.Point] = translatePoints(rightOrigo, rightCncPositioningPoints)
    leftRelativeHeatStartPoint: point.Point = translatePoint(leftOrigo, leftHeatStartPoint)
    rightRelativeHeatStartPoint: point.Point = translatePoint(rightOrigo, rightHeatStartPoint)

    # Logging
    # addIn.log("Left final profile points:")
    # for p2d in leftRelativeProfilePoints:
    #     addIn.log("Left "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Right final profile points:")
    # for p2d in rightRelativeProfilePoints:
    #     addIn.log("Right "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Left final positioning points:")
    # for p2d in leftRelativePositioningPoints:
    #     addIn.log("Left pos "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Right final positioning points:")
    # for p2d in rightRelativePositioningPoints:
    #     addIn.log("Right pos "  + str(p2d.x) + ", " + str(p2d.y) + ", " + str(p2d.z))
    # addIn.log("Left final Heat point "  + str(leftRelativeHeatStartPoint.x) + ", " + str(leftRelativeHeatStartPoint.y) + ", " + str(leftRelativeHeatStartPoint.z))
    # addIn.log("Right final Heat point "  + str(rightRelativeHeatStartPoint.x) + ", " + str(rightRelativeHeatStartPoint.y) + ", " + str(rightRelativeHeatStartPoint.z))

     # Sanity checks
    if (not len(leftRelativeProfilePoints) == len(rightRelativeProfilePoints)):
        utils.messageBox('Left and right relative profile point counts are not the same!')
        raise RuntimeError('Left and right relative profile point counts are not the same!')
    if (not len(leftRelativePositioningPoints) == len(rightRelativePositioningPoints)):
        utils.messageBox('Left and right relative positioning point counts are not the same!')
        raise RuntimeError('Left and right relative positioning point counts are not the same!')

    # Create the actual gcode
    try:
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'gcode.txt'), 'w') as file:
            gc = gcodeWriter.GcodeWriter(file)
            writePositioningPathGCode(gc, leftRelativePositioningPoints, rightRelativePositioningPoints, leftRelativeHeatStartPoint, rightRelativeHeatStartPoint)
            writeProfileCutGCode(gc, leftRelativeProfilePoints, rightRelativeProfilePoints)
            writeRetractPathGCode(gc, leftRelativePositioningPoints, rightRelativePositioningPoints, leftRelativeHeatStartPoint, rightRelativeHeatStartPoint)
    except:
        addIn.log(traceback.format_exc())
        utils.messageBox(traceback.format_exc())

# Translate 2D coordinates to a new origo.
# This is used to make the first point of the positioning path the origo, i.e. x=0, y=0
# And all the other points relative to the new origo.
def translatePoints(origo, points):
    translatedPoints = []
    for p in points:
        translatedPoints.append(translatePoint(origo, p))
    return translatedPoints

def translatePoint(origo, p):
    return point.Point(p.x - origo.x, p.y - origo.y, 0)

# Write gcode to position the wire at the start of the cut. This includes starting the heating of the wire.
def writePositioningPathGCode(gc: gcodeWriter.GcodeWriter, leftCncPositioningPoints: list[point.Point], rightCncPositioningPoints: list[point.Point], leftHeatStartPoint: point.Point, rightHeatStartPoint: point.Point):
    addIn.log('writePositioningPathGCode. Num points = ' + str(len(leftCncPositioningPoints)))
    isHeatOn = False
    isSpeedSet = False
    isStartPoint = False
    isLastPoint = False
    # Step through the points except the last one:
    # The last point coincides with the first profile point BUT when cutting the
    # profile part an offset may be used to compensate for the diameter of the 
    # track that is melted away. Therefore the positioning path stops shy of
    # its last point and the next movement will be to the first offset profile point instead.
    for i in range(0, len(leftCncPositioningPoints)-1):
        isStartPoint = i == 0
        isLastPoint = i == len(leftCncPositioningPoints)-2
        comment = ""
        if(isStartPoint):
            comment = "Start point of the positioning path"
        if(isLastPoint):
            comment = "Last point of the positioning path"

        if(not isHeatOn):
            gc.g0_linearMove(leftCncPositioningPoints[i], rightCncPositioningPoints[i], comment)
            if(not isSpeedSet):
                gc.f_speedCmPerSecond(addIn.attributes['cutSpeed'])
                isSpeedSet = True
            # About to enter the foam?
            # addIn.log("leftCncPositioningPoint[i] "  + str(leftCncPositioningPoints[i].x) + ", " + str(leftCncPositioningPoints[i].y) + ", " + str(leftCncPositioningPoints[i].z))
            # addIn.log("leftHeatStartPoint "  + str(leftHeatStartPoint.x) + ", " + str(leftHeatStartPoint.y) + ", " + str(leftHeatStartPoint.z))
            # addIn.log('leftCncPositioningPoints[i].equals(leftHeatStartPoint) = ' + str(leftCncPositioningPoints[i].equals(leftHeatStartPoint)))
            if(leftCncPositioningPoints[i].equals(leftHeatStartPoint) or rightCncPositioningPoints[i].equals(rightHeatStartPoint)):
                # Turn on heat
                gc.m3_heatPercent(addIn.attributes['heatPower'])
                # Pause three seconds
                gc.g4_pause(5)
                isHeatOn = True
        else:
            gc.g1_linearMove(leftCncPositioningPoints[i], rightCncPositioningPoints[i], comment)

   
# Write gcode for the profile cut. This code also compensates for the foam that is melted aways by offsetting the wire path from the desired profile.
def writeProfileCutGCode(gc: gcodeWriter.GcodeWriter, leftProfilePoints, rightProfilePoints):
    addIn.log('writeProfileCutGCode. Num leftProfilePoints/rightProfilePoints = ' + str(len(leftProfilePoints)) + '/' + str(len(rightProfilePoints)))
    isStartPoint = False
    isLastPoint = False

    # Offset the profile to compensate for the diameter of the 
    # track that is melted away
    logFunction = addIn.log
    leftProfilePointsOffset = expand.expand([point.Point(p.x, p.y) for p in leftProfilePoints], addIn.attributes['cutDiameter'] / 2, False)
    rightProfilePointsOffset = expand.expand([point.Point(p.x, p.y) for p in rightProfilePoints], addIn.attributes['cutDiameter'] / 2, False)
    addIn.log('writeProfileCutGCode. Num left Offset points = ' + str(len(leftProfilePointsOffset)))
    addIn.log('writeProfileCutGCode. Num right Offset points = ' + str(len(rightProfilePointsOffset)))
    # Move from current position, which is the (second to last) positioning
    # end point, to the first offset profile cut point and then continue around
    # the profile.
    for i in range(0, len(leftProfilePointsOffset)):
        isStartPoint = i == 0
        isLastPoint = i == len(leftProfilePointsOffset) -1
        comment = ""
        if(isStartPoint):
            comment = "First point of the profile"
        if(isLastPoint):
            comment = "Last point of the profile"
        addIn.log('index = ' + str(i))
        gc.g1_linearMove(leftProfilePointsOffset[i], rightProfilePointsOffset[i], comment)


# Write gcode to retract wire out of the foam after the profile cut. This includes shutting of the heat.
def writeRetractPathGCode(gc: gcodeWriter.GcodeWriter, leftCncPositioningPoints, rightCncPositioningPoints, leftHeatStartPoint, rightHeatStartPoint):
    addIn.log('writeRetractPathGCode. Num points = ' + str(len(leftCncPositioningPoints)))
    isHeatOn = True
    isStartPoint = False
    isLastPoint = False
    
    # Go the positioning path backwards.
    # The position when starting is at the last offset profile point.
    # Start to move to the second to last positioning point and continue
    retractStartIndex = len(leftCncPositioningPoints)-2
    for i in range(0, retractStartIndex +1):
        index = retractStartIndex - i
        isStartPoint = i == 0
        isLastPoint = i == retractStartIndex
        comment = ""
        if(isStartPoint):
            comment = "First point of the retraction path"
        if(isLastPoint):
            comment = "End point of the retraction path"
        if(isHeatOn):
            gc.g1_linearMove(leftCncPositioningPoints[index], rightCncPositioningPoints[index], comment)
            if(leftCncPositioningPoints[index].equals(leftHeatStartPoint) or rightCncPositioningPoints[index].equals(rightHeatStartPoint)):
                    # Turn off heat
                    gc.m5_heatOff()
                    isHeatOn = False
        else: # Heat is not on
            gc.g0_linearMove(leftCncPositioningPoints[index], rightCncPositioningPoints[index], comment)


# Check that the profiles start at the same point as the positioning paths end (= startPoint), and if they don't, reorder them so they do.
def reorderPointList(point2ds: list[point.Point], startPoint: point.Point) ->  list[point.Point]:
    newList: list[point.Point] = []
    indexOfNearestPoint = findIndexOfNearestPoint(point2ds, startPoint)
    if (indexOfNearestPoint == 0):
        newList = point2ds.copy()
    else:
        # Reorder. Throws exception if start point is not in point3ds
        addIn.log('Reordering point list. startIndex = ', indexOfNearestPoint)
        for i in range(0, len(point2ds)):
            if((i + indexOfNearestPoint) < len(point2ds)):
                oldIndex = i + indexOfNearestPoint
            else:
                oldIndex = i - indexOfNearestPoint
            newList[i] = point2ds[oldIndex]
    return newList

# Return the list index of the point in pointList that is nearest to targetPoint.
def findIndexOfNearestPoint(pointList: list[point.Point], targetPoint: point.Point) ->  int:
    diffList: list[: point.Point ] = []
    # Collect distances
    for p in pointList:
        diffList.append(getPointDiff(p, targetPoint))
    
    index = -1 # -1 means not found, but that is impossible!
    currIndex = 0
    minDiff = sys.float_info.max
    for diff in diffList:
        currDiff = diff.x * diff.x + diff.y * diff.y + diff.z * diff.z
        if(currDiff < minDiff):
            minDiff = currDiff
            index = currIndex
        currIndex = currIndex + 1
    return index

# Return a point that represents the differences in x, y , z  between p1 and p2.
# This is used to find the point nearest to another point.
def getPointDiff(p1: point.Point, p2: point.Point) -> point.Point:
    return point.Point(abs(p1.x-p2.x), abs(p1.y-p2.y), abs(p1.z-p2.z))


# Returns the points on the sketch.
def get3DPointsOnProfile(profileSketch)-> list[adsk.core.Point3D]:
    originPoint = profileSketch.originPoint
    sketchPoints = profileSketch.sketchPoints
    profile3DPoints = []
    for p in sketchPoints:
        # The origin point is always present. Do not add the origin point 
        if not p == originPoint:
            profile3DPoints.append(p.worldGeometry)
    return profile3DPoints


# Returns inifinite lines through pair-wise points on the left and right profile sketches.
def getProjectionLinesFromProfiles(leftProfileSketch: adsk.fusion.Sketch, rightProfileSketch: adsk.fusion.Sketch) -> list[adsk.core.InfiniteLine3D]:
    addIn.log('Getting projection lines for profiles')
    left3DPoints: list[adsk.core.Point3D] = get3DPointsOnProfile(leftProfileSketch)
    right3DPoints: list[adsk.core.Point3D] = get3DPointsOnProfile(rightProfileSketch)
    return getProjectionLinesFromPoint3ds(left3DPoints, right3DPoints)

# Returns inifinite lines through points on the left and right positioning path.
def getProjectionLinesFromPositioningPaths(leftPositioningPath: positioningPath.PositioningPath, rightPositioningPath: positioningPath.PositioningPath):
    addIn.log('Getting projection lines for positioning paths')
    return getProjectionLinesFromPoint3ds(leftPositioningPath.getPoint3ds(), rightPositioningPath.getPoint3ds())

# Returns inifinite lines through pair-wise corresponding points on the left and right profile planes.
# The inifinte lines are used to find their intersection with the CNC planes.
# To be able to do this:
# - The number of points on the left and right side must be the same.
# - The starting points must "correspond" in the meaning that the
# surface between them is a perfect straight line.
# - The following points must come in cutting order and must also correspond.
def getProjectionLinesFromPoint3ds(left3DPoints: list[adsk.core.Point3D], right3DPoints: list[adsk.core.Point3D]) -> list[adsk.core.InfiniteLine3D]:
    if not len(left3DPoints) == len(right3DPoints):
        addIn.log("Path point count is not same - cannot create projection lines. Left count = " + len(left3DPoints) + ", Right count = " + len(right3DPoints))
        utils.messageBox("Path point count is not same - cannot create projection lines. Left count = " + len(left3DPoints) + ", Right count = " + len(right3DPoints))
        raise RuntimeError(
            'Path point count is not same - cannot create projection lines')
    projectionLines = []
    # addIn.log("Creating projection lines for " + str(len(left3DPoints)) + " points")
    for i in range(0, len(right3DPoints)):
        left3DPoint =  left3DPoints[i]
        right3DPoint =  right3DPoints[i]
        # addIn.log("Creating projection line throught points for index " + str(i) + ":")
        projectionLines.append(getProjectionLineFromPoint3ds(left3DPoint, right3DPoint))
    return projectionLines


def getProjectionLineFromPoint3ds(left3DPoint: adsk.core.Point3D, right3DPoint: adsk.core.Point3D) -> adsk.core.InfiniteLine3D:
    # addIn.log("Creating projection line:")
    # addIn.log("Left projection point:"  + str(left3DPoint.x) + ", " + str(left3DPoint.y) + ", " + str(left3DPoint.z))
    # addIn.log("Right projection point:"  + str(right3DPoint.x) + ", " + str(right3DPoint.y) + ", " + str(right3DPoint.z))
    line3D = adsk.core.Line3D.create(left3DPoint, right3DPoint)
    infiniteLine = line3D.asInfiniteLine()
    return infiniteLine

# Get the intersection points for the (infinite)  projection lines and a CNC plane
def getIntersectionPoints(constructionPlane: adsk.fusion.ConstructionPlane, infiniteLines: adsk.core.InfiniteLine3D) -> list[adsk.core.Point3D]:
    intersectionPoints = []
    for infiniteLine in infiniteLines:
        intersectionPoints.append(getIntersectionPoint(constructionPlane, infiniteLine))
    return intersectionPoints

def getIntersectionPoint(constructionPlane: adsk.fusion.ConstructionPlane, infiniteLine: adsk.core.InfiniteLine3D) -> adsk.core.Point3D:
    return constructionPlane.geometry.intersectWithLine(infiniteLine)

# Create a spline profile from the point3ds on the given sketch
def createProfile(sketch: adsk.fusion.Sketch, point3ds: list[adsk.core.Point3D]):
    sketchPoints = adsk.core.ObjectCollection.create()
    addIn.log('createProfile')
    for p3D in point3ds:
        # addIn.log('point.x', p.x)
        # addIn.log('point.y', p.y)
        sketchPoints.add(sketch.modelToSketchSpace(p3D))
    sketch.sketchCurves.sketchFittedSplines.add(sketchPoints)

# Create lines connecting the given point3ds on the given sketch
def createLinePath(sketch: adsk.fusion.Sketch, point3ds: list[adsk.core.Point3D]):
    addIn.log('createLinePath')
    lenght = len(point3ds)
    if lenght > 1 :
        lines = sketch.sketchCurves.sketchLines
        for i in range(0, lenght -1):
            start = sketch.modelToSketchSpace(point3ds[i])
            end = sketch.modelToSketchSpace(point3ds[i+1])
            # addIn.log('creating line')
            lines.addByTwoPoints(start, end)
    else:
        addIn.log('No positioning path has been given. Add at least one line for the transition into the foam block')
        utils.messageBox('No positioning path has been given. Add at least one line for the transition into the foam block')

params = [
    addin.SelectionParam('leftCncPlane', 'ConstructionPlanes', 'Left (x,y) CNC coordinate plane', 'Select a plane', 1),
    addin.SelectionParam('leftPositioningPath', 'SketchLines', 'Left positioning path to profile', 'Select sketch lines to move the left cutting head to start position', 0),
    addin.SelectionParam('leftProfile', 'Profiles', 'Left side cutting profile', 'Select a profile', 1),
    addin.SelectionParam('rightPositioningPath', 'SketchLines', 'Right positioning path to profile', 'Select sketch lines to move the right cutting head to start position', 0),
    addin.SelectionParam('rightProfile', 'Profiles', 'Right side cutting profile', 'Select a profile', 1),
    addin.SelectionParam('rightCncPlane', 'ConstructionPlanes', 'Right (a,b) CNC coordinate plane', 'Select a plane', 1),
    addin.SelectionParam('foamBody', 'SolidBodies', 'The foam block to cut', 'Select a body representing the foam block to cut', 1),
    addin.NumberParam('cutDiameter', 'mm', '1 mm', 'hot wire cut diameter', 'Diameter of the cut due to wire temperature and cut speed, needed for radius compensation'),
    addin.NumberParam('cutSpeed', 'mm/min', '200 mm/min', 'cut speed', 'hot wire moving speed when cutting'),
    addin.NumberParam('heatPower', '', '50', 'heat power percent', 'Percent of full heat power 0-100')
]

addIn = addin.AddIn("wireCutterGcodeGenerator",
                    'WireCutterGcodeGenerator', params)
addIn.description = 'Projects airfoil profiles on CNC cutter 2D planes and generates gcode'

addIn.executor = executor
addIn.fieldValidator = fieldValidator

def selChanged(args):
    pass

addIn.ui.activeSelectionChanged.add(addIn.handlers.make_handler(
    adsk.core.ActiveSelectionEventHandler, selChanged))

def run(context):
    try:
        addIn.addButton()
    except:
        utils.messageBox(traceback.format_exc())

def stop(context):
    try:
        addIn.removeButton()
    except:
        utils.messageBox(traceback.format_exc())