# https://www.geeksforgeeks.org/graph-plotting-in-python-set-1/
# install matplotlib in order to use this program:
#   pip3 install matplotlib

import matplotlib.pyplot as plt

def plot(points, expandedPoints):
    # line 1 points
    x1, y1 = sepxy(points)
    # plotting the line 1 points
    plt.plot(x1, y1, label = "points")

    # line 2 points
    x2, y2 = sepxy(expandedPoints)
    # plotting the line 2 points
    plt.plot(x2, y2, label = "expanded")

    plt.gca().set_aspect('equal')
    
    # naming the x axis
    plt.xlabel('x - axis')
    # naming the y axis
    plt.ylabel('y - axis')
    # giving a title to my graph
    plt.title('points and expanded points')

    # show a legend on the plot
    plt.legend()

    # function to show the plot
    plt.show()

def sepxy(points):
    x = [p.x for p in points]
    y = [p.y for p in points]
    return x, y
