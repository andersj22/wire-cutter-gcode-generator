import sys
from airfoil import AirFoil
from point import Point

import argparse

parser = argparse.ArgumentParser(description='generate g-code for hot wire wing profile cutting')

parser.add_argument('foil_path', type=str,
                    help='path to foil dat file')
parser.add_argument('--width', type=float, default=100.0,
                    help='wing width (front to back) [mm]')
parser.add_argument('--vsafe', type=float, default=5.0,
                    help='safe vertical distance above top [mm]')
parser.add_argument('--vmarg', type=float, default=3.0,
                    help='top of wing level below 0 [mm]')
parser.add_argument('--cut_speed', type=float, default=300.0,
                    help='cut speed mm/min')


args = parser.parse_args()

foilPath = sys.argv[1]
foil = AirFoil(args.foil_path)

#print(f"(min={foil.coordRange['min']} max={foil.coordRange['max']})")

wingWidth = args.width
# make back to front edge go from 0 to wingWidth, make y go from 0 and down
# FIXME find back edge
# assume start x is back edge
# assume back x is > front x
# assume 0 <= x <= 1
maxX = foil.coordRange['max'].x
maxY = foil.coordRange['max'].y
points = []
for p in foil.points:
    points.append(Point(
        -(p.x - maxX) * wingWidth,
        (p.y - maxY) * wingWidth - args.vmarg
    ))

def coord(p):
    return '{0:.5f}'.format(p)

def coordsVert(p):
    return f"b{coord(p.y)} y{coord(p.y)}"

def coordsHor(p):
    return f"a{coord(p.x)} x{coord(p.x)}"

def coords(p):
    return f"{coordsHor(p)} {coordsVert(p)}"

safePos = Point(0, args.vsafe)
print(f"g0 {coordsVert(safePos)}")
print(f"g0 {coordsHor(safePos)}")
print(f"g1 f{args.cut_speed}")
for p in points:
    print(coords(p))
print(f"g1 {coordsVert(safePos)}")
