import re
import sys

from . import point

def log(x):
    # return None
    print(x, file=sys.stderr)

# Prints gcode commands and comments to a file
class GcodeWriter:
    def __init__(self, file):
       self.file = file

    # Basic writing
    def write(self,string):
        return self.file.write(string)

    def writeLine(self,string):
        return self.write(string + "\n")
    # New line
    def nl(self):
        return self.file.write("\n") 

    # Comments
    def comment(self, s):
        return self.writeLine(" ;" + s)
    # Comment with text and value
    def vc(self, s, v):
        return self.comment("{}: {}".format(s, v))

    # Write coordinate
    def wc(self, axis, value):
        axisLabel = ' ' + axis
        self.write(nv(axisLabel, value))
    
    def writeCoordinates(self, xValue, yValue, aValue, bValue, comment = ""):
        self.x(xValue)
        self.y(yValue)
        self.a(aValue)
        self.b(bValue)
        if( not comment):
            self.nl()
        else:    
            self.comment(comment)
    
    def x(self, value):
        self.wc(' x', value)
    
    def y(self, value):
        self.wc(' y', value)
    
    def a(self, value):
        self.wc(' a', value)
    
    def b(self, value):
        self.wc(' b', value)


    # Commands
    # Linear moves
    def g0_linearMoveTo(self, xValue: float, yValue: float,  aValue: float, bValue: float, comment = ""):
        self.write('g0')
        self.writeCoordinates(xValue, yValue, aValue, bValue, comment)
    
    def g1_linearMoveTo(self, xValue: float, yValue: float,  aValue: float, bValue: float, comment = ""):
        self.write('g1')
        self.writeCoordinates(xValue, yValue, aValue, bValue, comment)

    def g0_linearMove(self, leftPoint: point.Point, rightPoint: point.Point, comment = ""):
        self.g0_linearMoveTo(leftPoint.x, leftPoint.y, rightPoint.x, rightPoint.y, comment)
    
    def g1_linearMove(self, leftPoint: point.Point, rightPoint: point.Point, comment = ""):
        self.g1_linearMoveTo(leftPoint.x, leftPoint.y, rightPoint.x, rightPoint.y, comment)

    # Speed
    def f_speedMmPerMin(self, mmPerMin):
        self.write(f"f{round(mmPerMin)}")
        self.comment("Set speed to  " + str(round(mmPerMin)) + " mm per minute")
    
    def f_speedCmPerSecond(self, cmPerSecond):
        # Convert from fusion default cm/s to mm/min
        self.f_speedMmPerMin(cmPerSecond * 10 * 60)

    # Heat
    def m3_heatPromille(self, thousands):
        self.write("m3 s" + str(round(thousands)))
        self.comment("Set heat pwm pulse lenght to  " + str(round(thousands)) + " of maximum 1000")
    
    def m3_heatPercent(self, hundreds):
        self.m3_heatPromille(hundreds * 10)
    
    def m5_heatOff(self):
        self.write("m5")
        self.comment("Turn off heat")

    # Pause (dwell)
    def g4_pause(self, seconds):
        self.write('g4 p' + str(seconds))
        self.comment("Pause for " + str(seconds) + " seconds")
# Utilities        
def nv(n, v):
        return " {}{:.3f}".format(n, v * 10.0)
 