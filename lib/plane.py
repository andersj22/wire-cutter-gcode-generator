import sys

import adsk.core
import adsk.fusion

from . import point

def log(x):
    # return None
    print(x, file=sys.stderr)

# Represents a Plane, defined in a global 3D space.
# Represented by an origin (given in global coordinates) and two axis spanning the plane given in global coordinates.
# Can answer if a given global coordinate point is on the plane.
# Can convert the global point to plane coordinates, i.e. relative to the origin and given in plane coordinates
class Plane:
    def __init__(self, constructionPlane: adsk.fusion.ConstructionPlane, logFunction):
        self.log = logFunction
        plane = constructionPlane.geometry
        # Represent everything as vectors
        self.originVector: adsk.core.Vector3D = plane.origin.asVector()
        self.x: adsk.core.Vector3D = plane.uDirection
        self.y: adsk.core.Vector3D = plane.vDirection
        self.x.normalize()
        self.y.normalize()
        self.normal: adsk.core.Vector3D = plane.normal

    def isPointsOnPlane(self, point3ds: list[adsk.core.Point3D]):
        isOnPlane = True
        for p3d in point3ds:
            if not self.isPointOnPlane(p3d):
                isOnPlane = False
        return isOnPlane

    # point3d is a point on the plane if it (represented as a vector) has a zero dot product (AKA scalar product) with the planes normal.
    def isPointOnPlane(self, point3d: adsk.core.Point3D):
        isPointOnPlane = True
        if(not (point3d.asVector().dotProduct(self.normal) == 0)):
            self.log('ERROR: Found point supposedly on a plane which was not on that plane:')
            self.log('isPointOnPlane: point3d.asVector().dotProduct(self.normal) = ' + str(point3d.asVector().dotProduct(self.normal)))
            isPointOnPlane = False
        return isPointOnPlane

    # Returns a list of 2D points in plane coordinates for the given list of global 3D points.
    # All returned points have z=0.
    def getPointsInPlaneCoordinates(self, point3ds: list[adsk.core.Point3D]) -> list[point.Point]:
        relative2dPoints = []
        for p3d in point3ds:
            relative2dPoints.append(self.getPointInPlaneCoordinates(p3d))
        return relative2dPoints

    # Return the point3d in plane coordinates
    def getPointInPlaneCoordinates(self, point3d: adsk.core.Point3D)-> point.Point:
        pointVector: adsk.core.Vector3D = point3d.asVector()
        # Translate to remove the planes origo offset
        #self.log("Translate to remove the planes origo offset")
        #self.log("Origo: " + str(self.originVector.x) + ", " + str(self.originVector.y) + ", " + str(self.originVector.z))
        #self.log("Vector before translation: " + str(pointVector.x) + ", " + str(pointVector.y) + ", " + str(pointVector.z))
        pointVector.subtract(self.originVector)
        #self.log("Vector after translation: " + str(pointVector.x) + ", " + str(pointVector.y) + ", " + str(pointVector.z))
        # Project pointVector on the planes x and y axis
        #self.log("pointVector: ", pointVector)
        #self.log("self.x", self.x)
        #self.log("self.y", self.y)
        xscalar = pointVector.dotProduct(self.x)
        yscalar = pointVector.dotProduct(self.y)
        return point.Point(xscalar, yscalar, 0)




