import re
import sys

import adsk.core
import adsk.fusion

from .. import utils





class PositioningPath:
    # A positioning path is the path a CNC wire cutter takes to position itself at the start of the actual cut
    # It consists of a set of lines from a starting point to the first point of the cut.
    # A part of this path requires the wire to be heated since the wire travels through the foam.
    # To calculate where the heating must start the foam body is supplied.
    def __init__(self, pathLines: list[adsk.fusion.SketchLine], foamBody: adsk.fusion.BRepBody, logFunction):
        # points along the positioning path
        self.log = logFunction
        points: list[adsk.fusion.SketchPoint] = []
        startPoint = self.getStartPoint(pathLines)
        currPoint: adsk.fusion.SketchPoint = startPoint
        self.points3d: list[adsk.core.Point3D] = []
        self.foamBody = foamBody
        points.append(currPoint)
        for lineSegment in pathLines:
            # Logging start
            p3d = currPoint.worldGeometry
            #self.log("currPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
            p3d = lineSegment.startSketchPoint.worldGeometry
            #self.log("lineSegment.startSketchPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
            p3d = lineSegment.endSketchPoint.worldGeometry
            #self.log("lineSegment.endSketchPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
            # Logging end

            if lineSegment.startSketchPoint == currPoint:
                nextPoint = lineSegment.endSketchPoint
            elif lineSegment.endSketchPoint == currPoint:
                nextPoint = lineSegment.startSketchPoint
            else:
                utils.messageBox("The positioning path is not continuos. Each line must start at the end of the previous line.")
                raise RuntimeError(
                    f"The positioning path is not continuos. Each line must start at the end of the previous line.")

            points.append(nextPoint)

            currPoint = nextPoint

        # Convert sketchPoints to Point3D
        #self.log("Positioning path 3D points: ")
        for sketchPoint in points:
            p3d = sketchPoint.worldGeometry
            #self.log("pathPoint "  + str(p3d.x) + ", " + str(p3d.y) + ", " + str(p3d.z))
            self.points3d.append(p3d)

    # Find out what point is the first point of the path
    def getStartPoint(self, lines: list[adsk.fusion.SketchLine]) -> adsk.fusion.SketchPoint:
        # It is the start or end point of the first line
        firstLine = lines[0]
        # If the path consists of more than one line
        if len(lines) > 1:
            # It is the end point that is NOT connected to the next line.
            secondLine = lines[1]
            firstLineStart = firstLine.startSketchPoint
            firstLineEnd = firstLine.endSketchPoint
            if (firstLineStart == secondLine.startSketchPoint or firstLineStart == secondLine.endSketchPoint):
                startPoint = firstLineEnd
            elif (firstLineEnd == secondLine.startSketchPoint or firstLineEnd == secondLine.endSketchPoint):
                startPoint = firstLineStart
            else:
                utils.messageBox("The positioning path is not continuos. Each line must start at the end of the previous line.")
                raise RuntimeError(
                    f"The positioning path is not continuos. Each line must start at the end of the previous line.")
        else:
            # Only one line in the path. Go for the start point of that line.
            startPoint = firstLine.startSketchPoint
        return startPoint

        # Find out what point is the first point of the path
    def getHeatStartPointNumber(self):
        number = 0
        heatStartFound = False
        # Step through all points except the last one
        for i in range(0, len(self.points3d)-1):
            if(not heatStartFound):
                nextPoint3d = self.points3d[i+1]
                if(self.isPointInsideBody(nextPoint3d, self.foamBody)):
                    heatStartFound = True
                    number = i 
        # If point inside body is not found, return the last point as heat start    
        if(not heatStartFound):
            number = len(self.points3d)-1
        return number

    def getPoint3d(self, number)->adsk.core.Point3D:
        return self.points3d[number]


        # It is the start or end point of the first line
        firstLine = lines[0]
        # If the path consists of more than one line
        if len(lines) > 1:
            # It is the end point that is NOT connected to the next line.
            secondLine = lines[1]
            firstLineStart = firstLine.startSketchPoint
            firstLineEnd = firstLine.endSketchPoint
            if (firstLineStart == secondLine.startSketchPoint or firstLineStart == secondLine.endSketchPoint):
                startPoint = firstLineEnd
            elif (firstLineEnd == secondLine.startSketchPoint or firstLineEnd == secondLine.endSketchPoint):
                startPoint = firstLineStart
            else:
                utils.messageBox("The positioning path is not continuos. Each line must start at the end of the previous line.")
                raise RuntimeError(
                    f"The positioning path is not continuos. Each line must start at the end of the previous line.")
        else:
            # Only one line in the path. Go for the start point of that line.
            startPoint = firstLine.startSketchPoint
        return startPoint

    def isHeatStartPoint(self, point3d: adsk.core.Point3D) -> bool:
        return point3d == self.heatStartPoint3d

    def isPointInsideBody(self, point3d: adsk.core.Point3D, body: adsk.fusion.BRepBody):
        #self.log("isPointInsideBody")
        pointContainment: adsk.fusion.PointContainment = body.pointContainment(point3d)
        #self.log("PointContainment", pointContainment)
        #self.log("pointContainment == adsk.fusion.PointContainment.PointInsidePointContainment", pointContainment == adsk.fusion.PointContainment.PointInsidePointContainment)
        #self.log("pointContainment == adsk.fusion.PointContainment.PointOnPointContainment", pointContainment == adsk.fusion.PointContainment.PointOnPointContainment)
        return pointContainment == adsk.fusion.PointContainment.PointInsidePointContainment

    def getPoint3ds(self):
        return self.points3d

    def getLastPoint(self):
        return self.points3d[-1]

    # returns Dict with min x,y point and max x,y point
    # def calculateCoordRange(self, points):
    #     rangeMin = point.Point(sys.float_info.max, sys.float_info.max)
    #     rangeMax = point.Point(sys.float_info.min, sys.float_info.min)
    #     for p in points:
    #         rangeMin.x = min(rangeMin.x, p.x)
    #         rangeMin.y = min(rangeMin.y, p.y)
    #         rangeMax.x = max(rangeMax.x, p.x)
    #         rangeMax.y = max(rangeMax.y, p.y)
    #     return {"min": rangeMin, "max": rangeMax}
