if [ -z "$1" ]; then
	echo "missing input file path"
	exit 1
fi
for size in 16x16 32x32 64x64; do
	convert -background transparent "$1" -resize $size $size-normal.png
	cp $size-normal.png $size-disabled.png
done
